module Home exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Common exposing (..)


-- Home page


home : Model -> Html Msg
home model =
    div []
        [ p [] [ text "Vous pouvez soit créer une nouvelle partie, soit en rejoindre une." ]
        , label [ for "pseudo" ] [ text "Votre pseudo" ]
        , input [ type_ "text", id "pseudo", value (Maybe.withDefault "" model.pseudo), onInput Pseudo ] []
        , div [ class "choices" ]
            [ div []
                [ h2 [] [ text "Rejoindre une partie" ]
                , p [] [ text "Copiez le code qu'on vous a donné." ]
                , label [ for "gameId" ] [ text "Identifiant de la partie" ]
                , input [ type_ "number", id "gameId", value (Maybe.withDefault "" (Maybe.map toString model.game)), onInput GameId ] []
                , button
                    [ onClick (Send ("join " ++ (toString (Maybe.withDefault 0 model.game)) ++ " " ++ (Maybe.withDefault "" model.pseudo))), disabled <| not <| all [ is model.game, is model.pseudo ] ]
                    [ text "Rejoindre !" ]
                ]
            , div []
                [ h2 [] [ text "Nouvelle partie" ]
                , p [] [ text "Créez une nouvelle partie et invitez des amis à vous rejoindre !" ]
                , label [ for "maxPlayers", Html.Attributes.max "10" ] [ text "Nombre de joueurs" ]
                , input [ type_ "number", id "maxPlayers", value (toString model.maxPlayers), onInput MaxPlayer ] []
                , button [ onClick (Send ("new " ++ (toString model.maxPlayers))), disabled <| not <| all [ is model.pseudo ] ] [ text "C'est parti" ]
                ]
            ]
        ]


is : Maybe a -> Bool
is m =
    case m of
        Just _ ->
            True

        Nothing ->
            False


all : List Bool -> Bool
all =
    List.foldl (&&) True
