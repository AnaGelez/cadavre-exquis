module Game exposing (..)

import Common exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


game : Model -> Html Msg
game model =
    div []
        [ label [ for "words" ] [ text ("Écrivez " ++ model.rule ++ ".") ]
        , input [ id "words", onInput Response, value model.response ] []
        , button
            [ onClick (Send ("res " ++ (toString (Maybe.withDefault 0 model.game)) ++ " " ++ (Maybe.withDefault "" model.pseudo) ++ " -- " ++ model.response))
            , disabled (model.response == "")
            ]
            [ text "Envoyer" ]
        , h3 []
            [ text "On attend encore…" ]
        , ul []
            (List.map makeLi model.awaiting)
        ]


makeLi : String -> Html Msg
makeLi pseudo =
    li [] [ text pseudo ]
