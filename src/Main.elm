module Main exposing (..)

import Array exposing (..)
import Html exposing (..)
import Html.Attributes exposing (rel, href)
import WebSocket
import Json.Decode exposing (..)
import Common exposing (..)
import Home exposing (home)
import Lobby exposing (lobby)
import Game exposing (game)
import End exposing (end)


wsUrl =
    "ws://41.205.95.54:8080/api"


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


init : ( Model, Cmd Msg )
init =
    ( initialModel, Cmd.none )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Send msg ->
            ( { model | response = "" }, WebSocket.send wsUrl msg )

        Received msg ->
            let
                args =
                    Array.fromList (String.split " " msg)

                meth =
                    Maybe.withDefault "" (Array.get 0 args)
            in
                case meth of
                    "created" ->
                        update
                            (Send ("join " ++ (Maybe.withDefault "" (Array.get 1 args)) ++ " " ++ (Maybe.withDefault "" model.pseudo)))
                            { model | game = Result.toMaybe (String.toInt (Maybe.withDefault "" (Array.get 1 args))), players = [ (Maybe.withDefault "" model.pseudo) ] }

                    "joined" ->
                        let
                            otherArgs =
                                List.drop 1 (String.split " " msg)
                        in
                            ( { model | route = Lobby, players = otherArgs }, Cmd.none )

                    "round" ->
                        let
                            rule =
                                Maybe.withDefault "" (List.head (List.reverse (String.split " -- " msg)))
                        in
                            ( { model | rule = rule, route = Game, response = "" }, Cmd.none )

                    "awaiting" ->
                        let
                            otherArgs =
                                List.drop 1 (String.split " " msg)
                        in
                            ( { model | awaiting = otherArgs }, Cmd.none )

                    "end" ->
                        let
                            resStr =
                                String.dropLeft 4 msg

                            decodeRes =
                                decodeString (list (list resDecoder)) resStr

                            res =
                                Result.withDefault [] decodeRes
                        in
                            ( { model | route = End, res = res }, Cmd.none )

                    _ ->
                        ( model, Cmd.none )

        MaxPlayer m ->
            ( { model | maxPlayers = (Result.withDefault initialModel.maxPlayers (String.toInt m)) }, Cmd.none )

        GameId i ->
            ( { model | game = (Result.toMaybe (String.toInt i)) }, Cmd.none )

        Pseudo p ->
            -- Make it impossible to have spaces (it would break everything)
            ( { model | pseudo = List.head (String.split " " p) }, Cmd.none )

        Response res ->
            ( { model | response = res }, Cmd.none )

        Reset ->
            let
                pseudo =
                    model.pseudo
            in
                ( { initialModel | pseudo = pseudo }, Cmd.none )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    WebSocket.listen wsUrl Received



-- VIEW


view : Model -> Html Msg
view model =
    let
        content =
            case model.route of
                Home ->
                    home model

                Lobby ->
                    lobby model

                Game ->
                    game model

                End ->
                    end model
    in
        main_ []
            [ Html.node "link" [ rel "stylesheet", href "style.css" ] []
            , h1 [] [ text "Cadavre exquis" ]
            , content
            ]
