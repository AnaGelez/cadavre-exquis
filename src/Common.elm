module Common exposing (..)

import Json.Decode exposing (string, Decoder)
import Json.Decode.Pipeline exposing (decode, required)


type alias Id =
    Int


type Route
    = Home
    | Lobby
    | Game
    | End


type alias Model =
    { route : Route
    , game : Maybe Id
    , maxPlayers : Int
    , pseudo : Maybe String
    , players : List String
    , awaiting : List String
    , response : String
    , rule : String
    , res : List (List Res)
    }


initialModel : Model
initialModel =
    { route = Home
    , game = Nothing
    , maxPlayers = 3
    , pseudo = Nothing
    , players = []
    , awaiting = []
    , response = ""
    , rule = ""
    , res = []
    }


type Msg
    = Send String
    | Received String
    | MaxPlayer String
    | GameId String
    | Pseudo String
    | Response String
    | Reset


type alias Res =
    { words : String
    , player : String
    , color : String
    }


resDecoder : Decoder Res
resDecoder =
    decode Res
        |> required "words" string
        |> required "player" string
        |> required "color" string
