module End exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Common exposing (..)


end : Model -> Html Msg
end model =
    div []
        ([ h2 []
            [ text "Et voilà !" ]
         ]
            ++ List.map sentence model.res
            ++ [ button [ onClick Reset ] [ text "On en refait une !" ] ]
        )


sentence : List Res -> Html Msg
sentence resList =
    p [] ((List.intersperse (span [] [ text " " ]) (List.map words resList)) ++ [ text "." ])


words : Res -> Html Msg
words res =
    span
        [ title res.player
        , style
            [ ( "background", res.color )
            , ( "border-radius", "2px" )
            , ( "padding", "auto 5px" )
            ]
        ]
        [ text res.words ]
