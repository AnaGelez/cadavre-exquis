module Lobby exposing (..)

import Common exposing (..)
import Html exposing (..)


lobby : Model -> Html Msg
lobby model =
    div []
        [ p []
            [ text "La partie va bientôt commencer, il ne manque plus que d'autres joueurs" ]
        , p []
            [ text ("Vous pouvez invitez d'autres personnes en partageant l'identifiant de la partie, qui est « " ++ (toString (Maybe.withDefault 0 model.game)) ++ " ».") ]
        , h2 []
            [ text "Liste de joueurs connectés" ]
        , ul
            []
            (List.map
                (makeLi model)
                model.players
            )
        ]


makeLi : Model -> String -> Html Msg
makeLi model player =
    li []
        [ text
            (player
                ++ if player == (Maybe.withDefault "" model.pseudo) then
                    " (vous)"
                   else
                    ""
            )
        ]
