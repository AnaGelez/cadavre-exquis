require 'sinatra'
require 'sinatra-websocket'
require_relative 'game'

set :bind, '0.0.0.0'
set :games, []
set :last_id, 0

get '/' do
  send_file 'views/index.html'
end

get '/style.css' do
  send_file 'src/style.css'
end

get '/api' do
  if request.websocket?
    request.websocket do |ws|
      ws.onopen do
        puts 'New websocket connection'
      end
      ws.onmessage do |msg|
        process msg, settings, ws
      end
      ws.onclose do
        puts 'Websocket closed'
      end
    end
  else
    puts 'no websockt'
  end
end

def process (msg, settings, ws)
  puts 'Received:'
  puts msg
  args = msg.split
  method = args[0]
  case method
  when 'new'
    id = settings.last_id += 1
    max_players = args[1].to_i
    settings.games << Game.new(id, max_players)
    EM.next_tick { ws.send "created " + id.to_s }
  when 'join'
    game = settings.games[settings.games.index { |g| g.id == args[1].to_i }]
    game.add_player args[2], ws
    if game.players.length == game.max_players
      game.new_round
    end
  else
    game = settings.games[settings.games.index { |g| g.id == args[1].to_i }]
    game.add_result args[2], msg.split(' -- ')[1]
  end
end
