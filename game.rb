require 'json'

class Game
  attr_reader :id, :max_players, :players, :rules, :results, :round

  def initialize (id, max_players)
    @id = id
    @max_players = max_players
    @players = []
    @rules = get_rules
    @results = []
    @round = -1
  end

  def add_player (player, ws)
    @players << {
      name: player,
      color: get_color,
      ws: ws
    }
    send "joined " + ((@players.map { |p| p[:name] }) * ' ')
  end

  def send (msg)
    puts "sent : " + msg
    EM.next_tick { @players.each{ |p| p[:ws].send(msg) } }
  end

  def get_rules
    num = rand(3)
    case num
    when 0
      return rules_one
    when 1
      return rules_two
    else
      return rules_three
    end
  end

  def rules_one
    [ "un sujet", "un verbe", "un complément d'objet", "un lieu", "un moment" ]
  end

  def rules_two
    [ "un moment", "un sujet", "une extension du nom (adjectif, etc)", "un verbe", "un complément d'objet", "une manière (ex: calmement)", "un lieu" ]
  end

  def rules_three
    [ "un prénom", "un verbe", "un autre prénom", "un lieu", "une manière (ex: avec amour)", "un moment" ]
  end

  def add_result (player, words)
    if !@results[@round].any? { |res| res[:player] == player }
      @results[@round] << {
        words: words,
        player: player,
        color: (@players.detect { |p| p[:name] == player })[:color]
      }
    end

    if awaiting.length == 0
      new_round
    else
      send "awaiting " + (awaiting * ' ')
    end
  end

  def awaiting
    (@players.select { |p| !@results[@round].any? { |res| res[:player] == p[:name] } }).map { |p| p[:name]  }
  end

  def new_round
    @round += 1
    if @rules.length > round
      @results << []
      send 'round ' + @round.to_s + ' -- ' + @rules[@round]
      send "awaiting " + (awaiting * ' ')
    else
      send 'end ' + make_results.to_json
    end
  end

  def make_results
    res = []
    @max_players.times do |n|
      sentence = []
      @rules.length.times do |m|
        idx = m + n
        p = @players[idx % @max_players]
        add = @results[m].detect { |res| res[:player] == p[:name] }
        sentence << add
      end
      res << sentence
    end
    return res
  end

  def get_color
    colors = [ "#ffff3c", "#ffa369", "#5eafff", "#b081f9" ]
    colors[rand(colors.length)]
  end
end
